#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "radix.h"

void lookup_print(struct radix_node *root, char *key, int key_len) {
    char *results[3];
    int res = radix_lookup(root, (u8*)key, key_len, (void**)results, sizeof(results));

    printf("%.*s: #%d: ", key_len, key, res);

    int i = 0;
    for (; i < res; i++) {
        printf("%s ", results[i]);
    }
    printf("\n");
}

FILE *devrand = NULL;

#define PE_QUIT(x) do { \
    perror((x)); \
    exit(EXIT_FAILURE); \
} while (0)

void gen_random(char *data, size_t len) {
    if (devrand == NULL)
        devrand = fopen("/dev/urandom", "r");

    if (devrand == NULL) 
        PE_QUIT("Couldn't open /dev/urandom");

    size_t cur = 0;

    while (cur < len) {
        cur += fread(data + cur, 1, len - cur, devrand);

        if (feof(devrand))
            PE_QUIT("Unexpected end of file on /dev/urandom");
        if (ferror(devrand))
            PE_QUIT("Error reading from /dev/urandom");
    }

    // ASCII the randomness
    int i = 0;
    for (; i < len; i++) {
        // this isn't uniform but whatever
        char m = (char)((127 + (int)data[i]) % 93) + 33;
        data[i] = m;
    }
}

void check(bool should_be_true, char *msg_if_false, int index) {
    if (!should_be_true) {
        fprintf(stderr, "Error: #%d %s\n", index, msg_if_false);
        exit(EXIT_FAILURE);
    }
}

void stress_test(void) {
    struct radix_node *root = malloc(sizeof(struct radix_node));
    radix_init(root);

    const int len = 100000;
    const int sz = 32;
    int i = 0;
    char **key = malloc(sizeof(char*) * len);

    for (i = 0; i < len; i++) {
        key[i] = malloc(sz);
        gen_random(key[i], sz);
        radix_insert(root, (u8*)key[i], sz, malloc(sz), true);
    }

    check(root->num_nodes <= 256, "Number of nodes incorrect", 0);

    for (i = 0; i < len; i++) {
        radix_delete(root, (u8*)key[i], sz, NULL);
        gen_random(key[i], sz);
        radix_insert(root, (u8*)key[i], sz, malloc(sz), true);
    }

    check(root->num_nodes <= 256, "Number of nodes incorrect", 0);

    radix_delete(root, NULL, 0, NULL);

    for (i = 0; i < len; i++) {
        free(key[i]);
    }
    free(key);
    free(root);
    fclose(devrand);

    printf("Stress test complete\n");
}

#define T "t"
#define THR T "hr"
#define ONE "one"
#define TWO T "wo"
#define THREE THR "ee"
#define THRICE THR "ice"

int main(int argc, char **argv) {
    char *one = ONE, *two = TWO, *three = THREE, *thrice = THRICE;
    struct radix_node *root = malloc(sizeof(struct radix_node));

    radix_init(root);
    printf("Inserting \"%s\"\n", one);
    radix_insert(root, (u8*)one, strlen(one), one, false);
    lookup_print(root, one, strlen(one) - 1);

    printf("Inserting \"%s\"\n", two);
    radix_insert(root, (u8*)two, strlen(two), two, false);
    lookup_print(root, two, strlen(two));

    printf("Inserting \"%s\"\n", three);
    radix_insert(root, (u8*)three, strlen(three), three, false);
    printf("Inserting \"%s\"\n", thrice);
    radix_insert(root, (u8*)thrice, strlen(thrice), thrice, false);
    lookup_print(root, three, 1);
    lookup_print(root, three, 3);
    lookup_print(root, three, strlen(three));
    lookup_print(root, thrice, strlen(thrice));

    printf("Deleting \"%s\"\n", two);
    radix_delete(root, (u8*)two, strlen(two), two);
    lookup_print(root, two, 1);

    printf("Deleting \"th\", ptr == three\n");
    radix_delete(root, (u8*)three, 2, three);
    lookup_print(root, two, 1);
    lookup_print(root, one, 1);

    radix_delete(root, NULL, 0, NULL);
    free(root);

    if (argc >= 2 && !strcmp(argv[1], "stress")) {
        printf("Stress test!\n");
        stress_test();
    }

    return 0;
}

