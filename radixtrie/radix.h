#ifndef __DINET_RADIX_H
#define __DINET_RADIX_H

#define RADIX_NONE -1
#define RADIX_OK 0

typedef unsigned char u8;

struct radix_node {
    u8 *data;
    int len;
    struct radix_node **nodes;
    unsigned int num_nodes:8;
    bool free_ptr;
    void **ptr;
    int num_ptrs;
    size_t ptr_sz;
};

/**
 * radix_init
 *
 * Allocates a radix trie structure at the given pointer
 */
void radix_init(struct radix_node *root);


/**
 * radix_lookup
 *
 * Returns a list of all nodes starting with the given prefix, up to
 * max_results in result, and the number of nodes is returned.
 */
int radix_lookup(struct radix_node *root, u8 *prefix, int prefix_len, 
        void **result, int max_results);

/**
 * radix_insert
 *
 * Inserts a new node into the trie at the given key. Optionally frees the
 * associated data pointer when deleting
 */
int radix_insert(struct radix_node *root, u8 *key, int key_len, void *data, bool free_ptr);

/**
 * radix_delete
 *
 * Removes all nodes starting with given key and with the same data pointer.
 * If data pointer is NULL, all nodes starting with that key are deleted.
 *
 * Returns number of deleted nodes
 */
int radix_delete(struct radix_node *root, u8 *key, int key_len, void *data);

#endif

